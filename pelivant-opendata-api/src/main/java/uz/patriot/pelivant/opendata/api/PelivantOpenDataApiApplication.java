package uz.patriot.pelivant.opendata.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(value = {"uz.patriot.pelivant.db.repository"})
@EntityScan(value = {"uz.patriot.pelivant.db.domain"})
@SpringBootApplication(scanBasePackages = {"uz.patriot.pelivant.opendata.api", "uz.patriot.pelivant.db", "uz.patriot.pelivant.shared"})
public class PelivantOpenDataApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(PelivantOpenDataApiApplication.class, args);
    }
}

