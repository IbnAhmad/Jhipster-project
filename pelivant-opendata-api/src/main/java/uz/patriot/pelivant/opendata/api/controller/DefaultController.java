package uz.patriot.pelivant.opendata.api.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/opendata/api/default")
public class DefaultController {

    @Value("${current.profile}")
    private String currentProfile;

    @GetMapping("")
    public String get(){
        return String.format("CurrentProfile is %s. Now: %s", currentProfile, new Date()) ;
    }
}
