package uz.patriot.pelivant.api.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import uz.patriot.pelivant.api.repository.PlCourceServicesRepository;
import uz.patriot.pelivant.api.service.PlCourceServicesService;
import uz.patriot.pelivant.api.service.dto.PlCourceServicesDTO;
import uz.patriot.pelivant.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link uz.patriot.pelivant.api.domain.PlCourceServices}.
 */
@RestController
@RequestMapping("/api")
public class PlCourceServicesResource {

    private final Logger log = LoggerFactory.getLogger(PlCourceServicesResource.class);

    private static final String ENTITY_NAME = "plCourceServices";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PlCourceServicesService plCourceServicesService;

    private final PlCourceServicesRepository plCourceServicesRepository;

    public PlCourceServicesResource(
        PlCourceServicesService plCourceServicesService,
        PlCourceServicesRepository plCourceServicesRepository
    ) {
        this.plCourceServicesService = plCourceServicesService;
        this.plCourceServicesRepository = plCourceServicesRepository;
    }

    /**
     * {@code POST  /pl-cource-services} : Create a new plCourceServices.
     *
     * @param plCourceServicesDTO the plCourceServicesDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new plCourceServicesDTO, or with status {@code 400 (Bad Request)} if the plCourceServices has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pl-cource-services")
    public ResponseEntity<PlCourceServicesDTO> createPlCourceServices(@Valid @RequestBody PlCourceServicesDTO plCourceServicesDTO)
        throws URISyntaxException {
        log.debug("REST request to save PlCourceServices : {}", plCourceServicesDTO);
        if (plCourceServicesDTO.getId() != null) {
            throw new BadRequestAlertException("A new plCourceServices cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlCourceServicesDTO result = plCourceServicesService.save(plCourceServicesDTO);
        return ResponseEntity
            .created(new URI("/api/pl-cource-services/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pl-cource-services/:id} : Updates an existing plCourceServices.
     *
     * @param id the id of the plCourceServicesDTO to save.
     * @param plCourceServicesDTO the plCourceServicesDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plCourceServicesDTO,
     * or with status {@code 400 (Bad Request)} if the plCourceServicesDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the plCourceServicesDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pl-cource-services/{id}")
    public ResponseEntity<PlCourceServicesDTO> updatePlCourceServices(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PlCourceServicesDTO plCourceServicesDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PlCourceServices : {}, {}", id, plCourceServicesDTO);
        if (plCourceServicesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plCourceServicesDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!plCourceServicesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PlCourceServicesDTO result = plCourceServicesService.save(plCourceServicesDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plCourceServicesDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /pl-cource-services/:id} : Partial updates given fields of an existing plCourceServices, field will ignore if it is null
     *
     * @param id the id of the plCourceServicesDTO to save.
     * @param plCourceServicesDTO the plCourceServicesDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plCourceServicesDTO,
     * or with status {@code 400 (Bad Request)} if the plCourceServicesDTO is not valid,
     * or with status {@code 404 (Not Found)} if the plCourceServicesDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the plCourceServicesDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/pl-cource-services/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PlCourceServicesDTO> partialUpdatePlCourceServices(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PlCourceServicesDTO plCourceServicesDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PlCourceServices partially : {}, {}", id, plCourceServicesDTO);
        if (plCourceServicesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plCourceServicesDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!plCourceServicesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PlCourceServicesDTO> result = plCourceServicesService.partialUpdate(plCourceServicesDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plCourceServicesDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /pl-cource-services} : get all the plCourceServices.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of plCourceServices in body.
     */
    @GetMapping("/pl-cource-services")
    public ResponseEntity<List<PlCourceServicesDTO>> getAllPlCourceServices(Pageable pageable) {
        log.debug("REST request to get a page of PlCourceServices");
        Page<PlCourceServicesDTO> page = plCourceServicesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /pl-cource-services/:id} : get the "id" plCourceServices.
     *
     * @param id the id of the plCourceServicesDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the plCourceServicesDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pl-cource-services/{id}")
    public ResponseEntity<PlCourceServicesDTO> getPlCourceServices(@PathVariable Long id) {
        log.debug("REST request to get PlCourceServices : {}", id);
        Optional<PlCourceServicesDTO> plCourceServicesDTO = plCourceServicesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(plCourceServicesDTO);
    }

    /**
     * {@code DELETE  /pl-cource-services/:id} : delete the "id" plCourceServices.
     *
     * @param id the id of the plCourceServicesDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pl-cource-services/{id}")
    public ResponseEntity<Void> deletePlCourceServices(@PathVariable Long id) {
        log.debug("REST request to delete PlCourceServices : {}", id);
        plCourceServicesService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
