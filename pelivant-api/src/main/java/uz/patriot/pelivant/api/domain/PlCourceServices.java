package uz.patriot.pelivant.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A PlCourceServices.
 */
@Entity
@Table(name = "pl_cource_services")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PlCourceServices implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "mxik_code", nullable = false)
    private String mxikCode;

    @NotNull
    @Size(max = 200)
    @Column(name = "name", length = 200, nullable = false)
    private String name;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Integer isActive;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @NotNull
    @Column(name = "end_date", nullable = false)
    private LocalDate endDate;

    @NotNull
    @Column(name = "cost", nullable = false)
    private Double cost;

    @ManyToOne
    @JsonIgnoreProperties(value = { "courceIds", "regionIds", "districtIds", "companyId", "specId" }, allowSetters = true)
    private PlCources plCources;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PlCourceServices id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMxikCode() {
        return this.mxikCode;
    }

    public PlCourceServices mxikCode(String mxikCode) {
        this.setMxikCode(mxikCode);
        return this;
    }

    public void setMxikCode(String mxikCode) {
        this.mxikCode = mxikCode;
    }

    public String getName() {
        return this.name;
    }

    public PlCourceServices name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public PlCourceServices isActive(Integer isActive) {
        this.setIsActive(isActive);
        return this;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public LocalDate getStartDate() {
        return this.startDate;
    }

    public PlCourceServices startDate(LocalDate startDate) {
        this.setStartDate(startDate);
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return this.endDate;
    }

    public PlCourceServices endDate(LocalDate endDate) {
        this.setEndDate(endDate);
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Double getCost() {
        return this.cost;
    }

    public PlCourceServices cost(Double cost) {
        this.setCost(cost);
        return this;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public PlCources getPlCources() {
        return this.plCources;
    }

    public void setPlCources(PlCources plCources) {
        this.plCources = plCources;
    }

    public PlCourceServices plCources(PlCources plCources) {
        this.setPlCources(plCources);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlCourceServices)) {
            return false;
        }
        return id != null && id.equals(((PlCourceServices) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PlCourceServices{" +
            "id=" + getId() +
            ", mxikCode='" + getMxikCode() + "'" +
            ", name='" + getName() + "'" +
            ", isActive=" + getIsActive() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", cost=" + getCost() +
            "}";
    }
}
