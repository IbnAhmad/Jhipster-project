package uz.patriot.pelivant.api.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import uz.patriot.pelivant.api.service.dto.PlDistrictDTO;

import java.util.Optional;

/**
 * Service Interface for managing {@link uz.patriot.pelivant.api.domain.PlDistrict}.
 */
public interface PlDistrictService {
    /**
     * Save a plDistrict.
     *
     * @param plDistrictDTO the entity to save.
     * @return the persisted entity.
     */
    PlDistrictDTO save(PlDistrictDTO plDistrictDTO);

    /**
     * Partially updates a plDistrict.
     *
     * @param plDistrictDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PlDistrictDTO> partialUpdate(PlDistrictDTO plDistrictDTO);

    /**
     * Get all the plDistricts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PlDistrictDTO> findAll(Pageable pageable);

    /**
     * Get the "id" plDistrict.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PlDistrictDTO> findOne(Long id);

    /**
     * Delete the "id" plDistrict.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
