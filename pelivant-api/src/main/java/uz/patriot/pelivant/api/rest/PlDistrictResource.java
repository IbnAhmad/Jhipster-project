package uz.patriot.pelivant.api.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import uz.patriot.pelivant.api.repository.PlDistrictRepository;
import uz.patriot.pelivant.api.service.PlDistrictService;
import uz.patriot.pelivant.api.service.dto.PlDistrictDTO;
import uz.patriot.pelivant.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link uz.patriot.pelivant.api.domain.PlDistrict}.
 */
@RestController
@RequestMapping("/api")
public class PlDistrictResource {

    private final Logger log = LoggerFactory.getLogger(PlDistrictResource.class);

    private static final String ENTITY_NAME = "plDistrict";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PlDistrictService plDistrictService;

    private final PlDistrictRepository plDistrictRepository;

    public PlDistrictResource(PlDistrictService plDistrictService, PlDistrictRepository plDistrictRepository) {
        this.plDistrictService = plDistrictService;
        this.plDistrictRepository = plDistrictRepository;
    }

    /**
     * {@code POST  /pl-districts} : Create a new plDistrict.
     *
     * @param plDistrictDTO the plDistrictDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new plDistrictDTO, or with status {@code 400 (Bad Request)} if the plDistrict has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pl-districts")
    public ResponseEntity<PlDistrictDTO> createPlDistrict(@Valid @RequestBody PlDistrictDTO plDistrictDTO) throws URISyntaxException {
        log.debug("REST request to save PlDistrict : {}", plDistrictDTO);
        if (plDistrictDTO.getId() != null) {
            throw new BadRequestAlertException("A new plDistrict cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlDistrictDTO result = plDistrictService.save(plDistrictDTO);
        return ResponseEntity
            .created(new URI("/api/pl-districts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pl-districts/:id} : Updates an existing plDistrict.
     *
     * @param id the id of the plDistrictDTO to save.
     * @param plDistrictDTO the plDistrictDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plDistrictDTO,
     * or with status {@code 400 (Bad Request)} if the plDistrictDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the plDistrictDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pl-districts/{id}")
    public ResponseEntity<PlDistrictDTO> updatePlDistrict(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PlDistrictDTO plDistrictDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PlDistrict : {}, {}", id, plDistrictDTO);
        if (plDistrictDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plDistrictDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!plDistrictRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PlDistrictDTO result = plDistrictService.save(plDistrictDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plDistrictDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /pl-districts/:id} : Partial updates given fields of an existing plDistrict, field will ignore if it is null
     *
     * @param id the id of the plDistrictDTO to save.
     * @param plDistrictDTO the plDistrictDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plDistrictDTO,
     * or with status {@code 400 (Bad Request)} if the plDistrictDTO is not valid,
     * or with status {@code 404 (Not Found)} if the plDistrictDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the plDistrictDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/pl-districts/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PlDistrictDTO> partialUpdatePlDistrict(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PlDistrictDTO plDistrictDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PlDistrict partially : {}, {}", id, plDistrictDTO);
        if (plDistrictDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plDistrictDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!plDistrictRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PlDistrictDTO> result = plDistrictService.partialUpdate(plDistrictDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plDistrictDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /pl-districts} : get all the plDistricts.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of plDistricts in body.
     */
    @GetMapping("/pl-districts")
    public ResponseEntity<List<PlDistrictDTO>> getAllPlDistricts(Pageable pageable) {
        log.debug("REST request to get a page of PlDistricts");
        Page<PlDistrictDTO> page = plDistrictService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /pl-districts/:id} : get the "id" plDistrict.
     *
     * @param id the id of the plDistrictDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the plDistrictDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pl-districts/{id}")
    public ResponseEntity<PlDistrictDTO> getPlDistrict(@PathVariable Long id) {
        log.debug("REST request to get PlDistrict : {}", id);
        Optional<PlDistrictDTO> plDistrictDTO = plDistrictService.findOne(id);
        return ResponseUtil.wrapOrNotFound(plDistrictDTO);
    }

    /**
     * {@code DELETE  /pl-districts/:id} : delete the "id" plDistrict.
     *
     * @param id the id of the plDistrictDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pl-districts/{id}")
    public ResponseEntity<Void> deletePlDistrict(@PathVariable Long id) {
        log.debug("REST request to delete PlDistrict : {}", id);
        plDistrictService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
