package uz.patriot.pelivant.api.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link uz.patriot.pelivant.api.domain.PlCources} entity.
 */
public class PlCourcesDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer vacantCount;

    @NotNull
    @Size(max = 200)
    private String nameUz;

    @NotNull
    @Size(max = 2000)
    private String descriptionUz;

    @NotNull
    @Size(max = 200)
    private String nameRu;

    @NotNull
    @Size(max = 2000)
    private String descriptionRu;

    @NotNull
    private LocalDate startDate;

    @NotNull
    private LocalDate endDate;

    @NotNull
    private Integer isActive;

    private PlCompanyDTO companyId;

    private PlSpecialsDTO specId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVacantCount() {
        return vacantCount;
    }

    public void setVacantCount(Integer vacantCount) {
        this.vacantCount = vacantCount;
    }

    public String getNameUz() {
        return nameUz;
    }

    public void setNameUz(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getDescriptionUz() {
        return descriptionUz;
    }

    public void setDescriptionUz(String descriptionUz) {
        this.descriptionUz = descriptionUz;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getDescriptionRu() {
        return descriptionRu;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public PlCompanyDTO getCompanyId() {
        return companyId;
    }

    public void setCompanyId(PlCompanyDTO companyId) {
        this.companyId = companyId;
    }

    public PlSpecialsDTO getSpecId() {
        return specId;
    }

    public void setSpecId(PlSpecialsDTO specId) {
        this.specId = specId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlCourcesDTO)) {
            return false;
        }

        PlCourcesDTO plCourcesDTO = (PlCourcesDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, plCourcesDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PlCourcesDTO{" +
            "id=" + getId() +
            ", vacantCount=" + getVacantCount() +
            ", nameUz='" + getNameUz() + "'" +
            ", descriptionUz='" + getDescriptionUz() + "'" +
            ", nameRu='" + getNameRu() + "'" +
            ", descriptionRu='" + getDescriptionRu() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", isActive=" + getIsActive() +
            ", companyId=" + getCompanyId() +
            ", specId=" + getSpecId() +
            "}";
    }
}
