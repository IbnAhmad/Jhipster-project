package uz.patriot.pelivant.api.service.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import uz.patriot.pelivant.api.domain.PlCompany;
import uz.patriot.pelivant.api.service.dto.PlCompanyDTO;

/**
 * Mapper for the entity {@link PlCompany} and its DTO {@link PlCompanyDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PlCompanyMapper extends EntityMapper<PlCompanyDTO, PlCompany> {
    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PlCompanyDTO toDtoId(PlCompany plCompany);
}
