package uz.patriot.pelivant.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.patriot.pelivant.api.domain.PlSpecials;

/**
 * Spring Data SQL repository for the PlSpecials entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlSpecialsRepository extends JpaRepository<PlSpecials, Long> {}
