package uz.patriot.pelivant.api.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import uz.patriot.pelivant.api.repository.PlSpecialsRepository;
import uz.patriot.pelivant.api.service.PlSpecialsService;
import uz.patriot.pelivant.api.service.dto.PlSpecialsDTO;
import uz.patriot.pelivant.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link uz.patriot.pelivant.api.domain.PlSpecials}.
 */
@RestController
@RequestMapping("/api")
public class PlSpecialsResource {

    private final Logger log = LoggerFactory.getLogger(PlSpecialsResource.class);

    private static final String ENTITY_NAME = "plSpecials";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PlSpecialsService plSpecialsService;

    private final PlSpecialsRepository plSpecialsRepository;

    public PlSpecialsResource(PlSpecialsService plSpecialsService, PlSpecialsRepository plSpecialsRepository) {
        this.plSpecialsService = plSpecialsService;
        this.plSpecialsRepository = plSpecialsRepository;
    }

    /**
     * {@code POST  /pl-specials} : Create a new plSpecials.
     *
     * @param plSpecialsDTO the plSpecialsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new plSpecialsDTO, or with status {@code 400 (Bad Request)} if the plSpecials has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pl-specials")
    public ResponseEntity<PlSpecialsDTO> createPlSpecials(@Valid @RequestBody PlSpecialsDTO plSpecialsDTO) throws URISyntaxException {
        log.debug("REST request to save PlSpecials : {}", plSpecialsDTO);
        if (plSpecialsDTO.getId() != null) {
            throw new BadRequestAlertException("A new plSpecials cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlSpecialsDTO result = plSpecialsService.save(plSpecialsDTO);
        return ResponseEntity
            .created(new URI("/api/pl-specials/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pl-specials/:id} : Updates an existing plSpecials.
     *
     * @param id the id of the plSpecialsDTO to save.
     * @param plSpecialsDTO the plSpecialsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plSpecialsDTO,
     * or with status {@code 400 (Bad Request)} if the plSpecialsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the plSpecialsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pl-specials/{id}")
    public ResponseEntity<PlSpecialsDTO> updatePlSpecials(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PlSpecialsDTO plSpecialsDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PlSpecials : {}, {}", id, plSpecialsDTO);
        if (plSpecialsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plSpecialsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!plSpecialsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PlSpecialsDTO result = plSpecialsService.save(plSpecialsDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plSpecialsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /pl-specials/:id} : Partial updates given fields of an existing plSpecials, field will ignore if it is null
     *
     * @param id the id of the plSpecialsDTO to save.
     * @param plSpecialsDTO the plSpecialsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plSpecialsDTO,
     * or with status {@code 400 (Bad Request)} if the plSpecialsDTO is not valid,
     * or with status {@code 404 (Not Found)} if the plSpecialsDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the plSpecialsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/pl-specials/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PlSpecialsDTO> partialUpdatePlSpecials(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PlSpecialsDTO plSpecialsDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PlSpecials partially : {}, {}", id, plSpecialsDTO);
        if (plSpecialsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plSpecialsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!plSpecialsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PlSpecialsDTO> result = plSpecialsService.partialUpdate(plSpecialsDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plSpecialsDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /pl-specials} : get all the plSpecials.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of plSpecials in body.
     */
    @GetMapping("/pl-specials")
    public ResponseEntity<List<PlSpecialsDTO>> getAllPlSpecials(Pageable pageable) {
        log.debug("REST request to get a page of PlSpecials");
        Page<PlSpecialsDTO> page = plSpecialsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /pl-specials/:id} : get the "id" plSpecials.
     *
     * @param id the id of the plSpecialsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the plSpecialsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pl-specials/{id}")
    public ResponseEntity<PlSpecialsDTO> getPlSpecials(@PathVariable Long id) {
        log.debug("REST request to get PlSpecials : {}", id);
        Optional<PlSpecialsDTO> plSpecialsDTO = plSpecialsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(plSpecialsDTO);
    }

    /**
     * {@code DELETE  /pl-specials/:id} : delete the "id" plSpecials.
     *
     * @param id the id of the plSpecialsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pl-specials/{id}")
    public ResponseEntity<Void> deletePlSpecials(@PathVariable Long id) {
        log.debug("REST request to delete PlSpecials : {}", id);
        plSpecialsService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
