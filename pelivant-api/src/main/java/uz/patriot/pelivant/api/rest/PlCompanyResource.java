package uz.patriot.pelivant.api.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import uz.patriot.pelivant.api.repository.PlCompanyRepository;
import uz.patriot.pelivant.api.service.PlCompanyService;
import uz.patriot.pelivant.api.service.dto.PlCompanyDTO;
import uz.patriot.pelivant.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link uz.patriot.pelivant.api.domain.PlCompany}.
 */
@RestController
@RequestMapping("/api")
public class PlCompanyResource {

    private final Logger log = LoggerFactory.getLogger(PlCompanyResource.class);

    private static final String ENTITY_NAME = "plCompany";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PlCompanyService plCompanyService;

    private final PlCompanyRepository plCompanyRepository;

    public PlCompanyResource(PlCompanyService plCompanyService, PlCompanyRepository plCompanyRepository) {
        this.plCompanyService = plCompanyService;
        this.plCompanyRepository = plCompanyRepository;
    }

    /**
     * {@code POST  /pl-companies} : Create a new plCompany.
     *
     * @param plCompanyDTO the plCompanyDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new plCompanyDTO, or with status {@code 400 (Bad Request)} if the plCompany has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pl-companies")
    public ResponseEntity<PlCompanyDTO> createPlCompany(@Valid @RequestBody PlCompanyDTO plCompanyDTO) throws URISyntaxException {
        log.debug("REST request to save PlCompany : {}", plCompanyDTO);
        if (plCompanyDTO.getId() != null) {
            throw new BadRequestAlertException("A new plCompany cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlCompanyDTO result = plCompanyService.save(plCompanyDTO);
        return ResponseEntity
            .created(new URI("/api/pl-companies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pl-companies/:id} : Updates an existing plCompany.
     *
     * @param id the id of the plCompanyDTO to save.
     * @param plCompanyDTO the plCompanyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plCompanyDTO,
     * or with status {@code 400 (Bad Request)} if the plCompanyDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the plCompanyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pl-companies/{id}")
    public ResponseEntity<PlCompanyDTO> updatePlCompany(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PlCompanyDTO plCompanyDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PlCompany : {}, {}", id, plCompanyDTO);
        if (plCompanyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plCompanyDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!plCompanyRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PlCompanyDTO result = plCompanyService.save(plCompanyDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plCompanyDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /pl-companies/:id} : Partial updates given fields of an existing plCompany, field will ignore if it is null
     *
     * @param id the id of the plCompanyDTO to save.
     * @param plCompanyDTO the plCompanyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plCompanyDTO,
     * or with status {@code 400 (Bad Request)} if the plCompanyDTO is not valid,
     * or with status {@code 404 (Not Found)} if the plCompanyDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the plCompanyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/pl-companies/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PlCompanyDTO> partialUpdatePlCompany(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PlCompanyDTO plCompanyDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PlCompany partially : {}, {}", id, plCompanyDTO);
        if (plCompanyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plCompanyDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!plCompanyRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PlCompanyDTO> result = plCompanyService.partialUpdate(plCompanyDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plCompanyDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /pl-companies} : get all the plCompanies.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of plCompanies in body.
     */
    @GetMapping("/pl-companies")
    public ResponseEntity<List<PlCompanyDTO>> getAllPlCompanies(Pageable pageable) {
        log.debug("REST request to get a page of PlCompanies");
        Page<PlCompanyDTO> page = plCompanyService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /pl-companies/:id} : get the "id" plCompany.
     *
     * @param id the id of the plCompanyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the plCompanyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pl-companies/{id}")
    public ResponseEntity<PlCompanyDTO> getPlCompany(@PathVariable Long id) {
        log.debug("REST request to get PlCompany : {}", id);
        Optional<PlCompanyDTO> plCompanyDTO = plCompanyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(plCompanyDTO);
    }

    /**
     * {@code DELETE  /pl-companies/:id} : delete the "id" plCompany.
     *
     * @param id the id of the plCompanyDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pl-companies/{id}")
    public ResponseEntity<Void> deletePlCompany(@PathVariable Long id) {
        log.debug("REST request to delete PlCompany : {}", id);
        plCompanyService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
