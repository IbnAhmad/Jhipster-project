package uz.patriot.pelivant.api.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import uz.patriot.pelivant.api.repository.PlRegionRepository;
import uz.patriot.pelivant.api.service.PlRegionService;
import uz.patriot.pelivant.api.service.dto.PlRegionDTO;
import uz.patriot.pelivant.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link uz.patriot.pelivant.api.domain.PlRegion}.
 */
@RestController
@RequestMapping("/api")
public class PlRegionResource {

    private final Logger log = LoggerFactory.getLogger(PlRegionResource.class);

    private static final String ENTITY_NAME = "plRegion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PlRegionService plRegionService;

    private final PlRegionRepository plRegionRepository;

    public PlRegionResource(PlRegionService plRegionService, PlRegionRepository plRegionRepository) {
        this.plRegionService = plRegionService;
        this.plRegionRepository = plRegionRepository;
    }

    /**
     * {@code POST  /pl-regions} : Create a new plRegion.
     *
     * @param plRegionDTO the plRegionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new plRegionDTO, or with status {@code 400 (Bad Request)} if the plRegion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pl-regions")
    public ResponseEntity<PlRegionDTO> createPlRegion(@Valid @RequestBody PlRegionDTO plRegionDTO) throws URISyntaxException {
        log.debug("REST request to save PlRegion : {}", plRegionDTO);
        if (plRegionDTO.getId() != null) {
            throw new BadRequestAlertException("A new plRegion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlRegionDTO result = plRegionService.save(plRegionDTO);
        return ResponseEntity
            .created(new URI("/api/pl-regions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pl-regions/:id} : Updates an existing plRegion.
     *
     * @param id the id of the plRegionDTO to save.
     * @param plRegionDTO the plRegionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plRegionDTO,
     * or with status {@code 400 (Bad Request)} if the plRegionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the plRegionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pl-regions/{id}")
    public ResponseEntity<PlRegionDTO> updatePlRegion(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PlRegionDTO plRegionDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PlRegion : {}, {}", id, plRegionDTO);
        if (plRegionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plRegionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!plRegionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PlRegionDTO result = plRegionService.save(plRegionDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plRegionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /pl-regions/:id} : Partial updates given fields of an existing plRegion, field will ignore if it is null
     *
     * @param id the id of the plRegionDTO to save.
     * @param plRegionDTO the plRegionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plRegionDTO,
     * or with status {@code 400 (Bad Request)} if the plRegionDTO is not valid,
     * or with status {@code 404 (Not Found)} if the plRegionDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the plRegionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/pl-regions/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PlRegionDTO> partialUpdatePlRegion(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PlRegionDTO plRegionDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PlRegion partially : {}, {}", id, plRegionDTO);
        if (plRegionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plRegionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!plRegionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PlRegionDTO> result = plRegionService.partialUpdate(plRegionDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plRegionDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /pl-regions} : get all the plRegions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of plRegions in body.
     */
    @GetMapping("/pl-regions")
    public ResponseEntity<List<PlRegionDTO>> getAllPlRegions(Pageable pageable) {
        log.debug("REST request to get a page of PlRegions");
        Page<PlRegionDTO> page = plRegionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /pl-regions/:id} : get the "id" plRegion.
     *
     * @param id the id of the plRegionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the plRegionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pl-regions/{id}")
    public ResponseEntity<PlRegionDTO> getPlRegion(@PathVariable Long id) {
        log.debug("REST request to get PlRegion : {}", id);
        Optional<PlRegionDTO> plRegionDTO = plRegionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(plRegionDTO);
    }

    /**
     * {@code DELETE  /pl-regions/:id} : delete the "id" plRegion.
     *
     * @param id the id of the plRegionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pl-regions/{id}")
    public ResponseEntity<Void> deletePlRegion(@PathVariable Long id) {
        log.debug("REST request to delete PlRegion : {}", id);
        plRegionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
