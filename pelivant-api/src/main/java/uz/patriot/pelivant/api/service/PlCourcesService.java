package uz.patriot.pelivant.api.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import uz.patriot.pelivant.api.service.dto.PlCourcesDTO;

import java.util.Optional;

/**
 * Service Interface for managing {@link uz.patriot.pelivant.api.domain.PlCources}.
 */
public interface PlCourcesService {
    /**
     * Save a plCources.
     *
     * @param plCourcesDTO the entity to save.
     * @return the persisted entity.
     */
    PlCourcesDTO save(PlCourcesDTO plCourcesDTO);

    /**
     * Partially updates a plCources.
     *
     * @param plCourcesDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PlCourcesDTO> partialUpdate(PlCourcesDTO plCourcesDTO);

    /**
     * Get all the plCources.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PlCourcesDTO> findAll(Pageable pageable);

    /**
     * Get the "id" plCources.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PlCourcesDTO> findOne(Long id);

    /**
     * Delete the "id" plCources.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
