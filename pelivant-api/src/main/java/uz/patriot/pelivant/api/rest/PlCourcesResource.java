package uz.patriot.pelivant.api.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import uz.patriot.pelivant.api.repository.PlCourcesRepository;
import uz.patriot.pelivant.api.service.PlCourcesService;
import uz.patriot.pelivant.api.service.dto.PlCourcesDTO;
import uz.patriot.pelivant.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link uz.patriot.pelivant.api.domain.PlCources}.
 */
@RestController
@RequestMapping("/api")
public class PlCourcesResource {

    private final Logger log = LoggerFactory.getLogger(PlCourcesResource.class);

    private static final String ENTITY_NAME = "plCources";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PlCourcesService plCourcesService;

    private final PlCourcesRepository plCourcesRepository;

    public PlCourcesResource(PlCourcesService plCourcesService, PlCourcesRepository plCourcesRepository) {
        this.plCourcesService = plCourcesService;
        this.plCourcesRepository = plCourcesRepository;
    }

    /**
     * {@code POST  /pl-cources} : Create a new plCources.
     *
     * @param plCourcesDTO the plCourcesDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new plCourcesDTO, or with status {@code 400 (Bad Request)} if the plCources has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pl-cources")
    public ResponseEntity<PlCourcesDTO> createPlCources(@Valid @RequestBody PlCourcesDTO plCourcesDTO) throws URISyntaxException {
        log.debug("REST request to save PlCources : {}", plCourcesDTO);
        if (plCourcesDTO.getId() != null) {
            throw new BadRequestAlertException("A new plCources cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlCourcesDTO result = plCourcesService.save(plCourcesDTO);
        return ResponseEntity
            .created(new URI("/api/pl-cources/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pl-cources/:id} : Updates an existing plCources.
     *
     * @param id the id of the plCourcesDTO to save.
     * @param plCourcesDTO the plCourcesDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plCourcesDTO,
     * or with status {@code 400 (Bad Request)} if the plCourcesDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the plCourcesDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pl-cources/{id}")
    public ResponseEntity<PlCourcesDTO> updatePlCources(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PlCourcesDTO plCourcesDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PlCources : {}, {}", id, plCourcesDTO);
        if (plCourcesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plCourcesDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!plCourcesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PlCourcesDTO result = plCourcesService.save(plCourcesDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plCourcesDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /pl-cources/:id} : Partial updates given fields of an existing plCources, field will ignore if it is null
     *
     * @param id the id of the plCourcesDTO to save.
     * @param plCourcesDTO the plCourcesDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plCourcesDTO,
     * or with status {@code 400 (Bad Request)} if the plCourcesDTO is not valid,
     * or with status {@code 404 (Not Found)} if the plCourcesDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the plCourcesDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/pl-cources/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PlCourcesDTO> partialUpdatePlCources(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PlCourcesDTO plCourcesDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PlCources partially : {}, {}", id, plCourcesDTO);
        if (plCourcesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plCourcesDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!plCourcesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PlCourcesDTO> result = plCourcesService.partialUpdate(plCourcesDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plCourcesDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /pl-cources} : get all the plCources.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of plCources in body.
     */
    @GetMapping("/pl-cources")
    public ResponseEntity<List<PlCourcesDTO>> getAllPlCources(Pageable pageable) {
        log.debug("REST request to get a page of PlCources");
        Page<PlCourcesDTO> page = plCourcesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /pl-cources/:id} : get the "id" plCources.
     *
     * @param id the id of the plCourcesDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the plCourcesDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pl-cources/{id}")
    public ResponseEntity<PlCourcesDTO> getPlCources(@PathVariable Long id) {
        log.debug("REST request to get PlCources : {}", id);
        Optional<PlCourcesDTO> plCourcesDTO = plCourcesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(plCourcesDTO);
    }

    /**
     * {@code DELETE  /pl-cources/:id} : delete the "id" plCources.
     *
     * @param id the id of the plCourcesDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pl-cources/{id}")
    public ResponseEntity<Void> deletePlCources(@PathVariable Long id) {
        log.debug("REST request to delete PlCources : {}", id);
        plCourcesService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
