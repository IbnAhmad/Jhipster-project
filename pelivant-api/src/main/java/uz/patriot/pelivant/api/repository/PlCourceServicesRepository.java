package uz.patriot.pelivant.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.patriot.pelivant.api.domain.PlCourceServices;

/**
 * Spring Data SQL repository for the PlCourceServices entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlCourceServicesRepository extends JpaRepository<PlCourceServices, Long> {}
