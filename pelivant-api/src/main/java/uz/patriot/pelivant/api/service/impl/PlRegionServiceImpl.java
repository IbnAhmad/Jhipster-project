package uz.patriot.pelivant.api.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.patriot.pelivant.api.domain.PlRegion;
import uz.patriot.pelivant.api.repository.PlRegionRepository;
import uz.patriot.pelivant.api.service.PlRegionService;
import uz.patriot.pelivant.api.service.dto.PlRegionDTO;
import uz.patriot.pelivant.api.service.mapper.PlRegionMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PlRegion}.
 */
@Service
@Transactional
public class PlRegionServiceImpl implements PlRegionService {

    private final Logger log = LoggerFactory.getLogger(PlRegionServiceImpl.class);

    private final PlRegionRepository plRegionRepository;

    private final PlRegionMapper plRegionMapper;

    public PlRegionServiceImpl(PlRegionRepository plRegionRepository, PlRegionMapper plRegionMapper) {
        this.plRegionRepository = plRegionRepository;
        this.plRegionMapper = plRegionMapper;
    }

    @Override
    public PlRegionDTO save(PlRegionDTO plRegionDTO) {
        log.debug("Request to save PlRegion : {}", plRegionDTO);
        PlRegion plRegion = plRegionMapper.toEntity(plRegionDTO);
        plRegion = plRegionRepository.save(plRegion);
        return plRegionMapper.toDto(plRegion);
    }

    @Override
    public Optional<PlRegionDTO> partialUpdate(PlRegionDTO plRegionDTO) {
        log.debug("Request to partially update PlRegion : {}", plRegionDTO);

        return plRegionRepository
            .findById(plRegionDTO.getId())
            .map(existingPlRegion -> {
                plRegionMapper.partialUpdate(existingPlRegion, plRegionDTO);

                return existingPlRegion;
            })
            .map(plRegionRepository::save)
            .map(plRegionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PlRegionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PlRegions");
        return plRegionRepository.findAll(pageable).map(plRegionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PlRegionDTO> findOne(Long id) {
        log.debug("Request to get PlRegion : {}", id);
        return plRegionRepository.findById(id).map(plRegionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlRegion : {}", id);
        plRegionRepository.deleteById(id);
    }
}
