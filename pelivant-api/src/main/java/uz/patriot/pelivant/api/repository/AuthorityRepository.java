package uz.patriot.pelivant.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.patriot.pelivant.api.domain.Authority;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {}
