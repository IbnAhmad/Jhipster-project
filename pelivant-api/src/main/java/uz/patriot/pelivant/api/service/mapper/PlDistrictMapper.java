package uz.patriot.pelivant.api.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import uz.patriot.pelivant.api.domain.PlDistrict;
import uz.patriot.pelivant.api.service.dto.PlDistrictDTO;

/**
 * Mapper for the entity {@link PlDistrict} and its DTO {@link PlDistrictDTO}.
 */
@Mapper(componentModel = "spring", uses = { PlRegionMapper.class, PlCourcesMapper.class })
public interface PlDistrictMapper extends EntityMapper<PlDistrictDTO, PlDistrict> {
    @Mapping(target = "plRegion", source = "plRegion", qualifiedByName = "id")
    @Mapping(target = "plCources", source = "plCources", qualifiedByName = "id")
    PlDistrictDTO toDto(PlDistrict s);
}
