/**
 * View Models used by Spring MVC REST controllers.
 */
package uz.patriot.pelivant.api.rest.vm;
