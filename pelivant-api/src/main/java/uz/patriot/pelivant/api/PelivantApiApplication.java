package uz.patriot.pelivant.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(value = {"uz.patriot.pelivant.db.repository"})
@EntityScan(value = {"uz.patriot.pelivant.db.domain"})
@SpringBootApplication(scanBasePackages = {"uz.patriot.pelivant.api", "uz.patriot.pelivant.db", "uz.patriot.pelivant.shared"})
public class PelivantApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(PelivantApiApplication.class, args);
    }
}
