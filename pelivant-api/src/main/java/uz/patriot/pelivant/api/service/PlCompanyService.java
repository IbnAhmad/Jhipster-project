package uz.patriot.pelivant.api.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import uz.patriot.pelivant.api.service.dto.PlCompanyDTO;

import java.util.Optional;

/**
 * Service Interface for managing {@link uz.patriot.pelivant.api.domain.PlCompany}.
 */
public interface PlCompanyService {
    /**
     * Save a plCompany.
     *
     * @param plCompanyDTO the entity to save.
     * @return the persisted entity.
     */
    PlCompanyDTO save(PlCompanyDTO plCompanyDTO);

    /**
     * Partially updates a plCompany.
     *
     * @param plCompanyDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PlCompanyDTO> partialUpdate(PlCompanyDTO plCompanyDTO);

    /**
     * Get all the plCompanies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PlCompanyDTO> findAll(Pageable pageable);

    /**
     * Get the "id" plCompany.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PlCompanyDTO> findOne(Long id);

    /**
     * Delete the "id" plCompany.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
