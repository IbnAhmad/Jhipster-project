package uz.patriot.pelivant.api.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import uz.patriot.pelivant.api.service.dto.PlRegionDTO;

import java.util.Optional;

/**
 * Service Interface for managing {@link uz.patriot.pelivant.api.domain.PlRegion}.
 */
public interface PlRegionService {
    /**
     * Save a plRegion.
     *
     * @param plRegionDTO the entity to save.
     * @return the persisted entity.
     */
    PlRegionDTO save(PlRegionDTO plRegionDTO);

    /**
     * Partially updates a plRegion.
     *
     * @param plRegionDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PlRegionDTO> partialUpdate(PlRegionDTO plRegionDTO);

    /**
     * Get all the plRegions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PlRegionDTO> findAll(Pageable pageable);

    /**
     * Get the "id" plRegion.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PlRegionDTO> findOne(Long id);

    /**
     * Delete the "id" plRegion.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
