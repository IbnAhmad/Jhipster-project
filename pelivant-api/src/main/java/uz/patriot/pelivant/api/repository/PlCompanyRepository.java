package uz.patriot.pelivant.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.patriot.pelivant.api.domain.PlCompany;

/**
 * Spring Data SQL repository for the PlCompany entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlCompanyRepository extends JpaRepository<PlCompany, Long> {}
