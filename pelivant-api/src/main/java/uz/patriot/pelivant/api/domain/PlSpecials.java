package uz.patriot.pelivant.api.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * A PlSpecials.
 */
@Entity
@Table(name = "pl_specials")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PlSpecials implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(max = 200)
    @Column(name = "name_uz", length = 200, nullable = false)
    private String nameUz;

    @NotNull
    @Size(max = 2000)
    @Column(name = "description_uz", length = 2000, nullable = false)
    private String descriptionUz;

    @NotNull
    @Size(max = 200)
    @Column(name = "name_ru", length = 200, nullable = false)
    private String nameRu;

    @NotNull
    @Size(max = 2000)
    @Column(name = "description_ru", length = 2000, nullable = false)
    private String descriptionRu;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PlSpecials id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameUz() {
        return this.nameUz;
    }

    public PlSpecials nameUz(String nameUz) {
        this.setNameUz(nameUz);
        return this;
    }

    public void setNameUz(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getDescriptionUz() {
        return this.descriptionUz;
    }

    public PlSpecials descriptionUz(String descriptionUz) {
        this.setDescriptionUz(descriptionUz);
        return this;
    }

    public void setDescriptionUz(String descriptionUz) {
        this.descriptionUz = descriptionUz;
    }

    public String getNameRu() {
        return this.nameRu;
    }

    public PlSpecials nameRu(String nameRu) {
        this.setNameRu(nameRu);
        return this;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getDescriptionRu() {
        return this.descriptionRu;
    }

    public PlSpecials descriptionRu(String descriptionRu) {
        this.setDescriptionRu(descriptionRu);
        return this;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlSpecials)) {
            return false;
        }
        return id != null && id.equals(((PlSpecials) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PlSpecials{" +
            "id=" + getId() +
            ", nameUz='" + getNameUz() + "'" +
            ", descriptionUz='" + getDescriptionUz() + "'" +
            ", nameRu='" + getNameRu() + "'" +
            ", descriptionRu='" + getDescriptionRu() + "'" +
            "}";
    }
}
