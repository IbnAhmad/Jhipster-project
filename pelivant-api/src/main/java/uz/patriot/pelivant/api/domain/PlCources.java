package uz.patriot.pelivant.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A PlCources.
 */
@Entity
@Table(name = "pl_cources")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PlCources implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "vacant_count", nullable = false)
    private Integer vacantCount;

    @NotNull
    @Size(max = 200)
    @Column(name = "name_uz", length = 200, nullable = false)
    private String nameUz;

    @NotNull
    @Size(max = 2000)
    @Column(name = "description_uz", length = 2000, nullable = false)
    private String descriptionUz;

    @NotNull
    @Size(max = 200)
    @Column(name = "name_ru", length = 200, nullable = false)
    private String nameRu;

    @NotNull
    @Size(max = 2000)
    @Column(name = "description_ru", length = 2000, nullable = false)
    private String descriptionRu;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @NotNull
    @Column(name = "end_date", nullable = false)
    private LocalDate endDate;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Integer isActive;

    @OneToMany(mappedBy = "plCources")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "plCources" }, allowSetters = true)
    private Set<PlCourceServices> courceIds = new HashSet<>();

    @OneToMany(mappedBy = "plCources")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "regionIds", "plCources" }, allowSetters = true)
    private Set<PlRegion> regionIds = new HashSet<>();

    @OneToMany(mappedBy = "plCources")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "plRegion", "plCources" }, allowSetters = true)
    private Set<PlDistrict> districtIds = new HashSet<>();

    @ManyToOne
    private PlCompany companyId;

    @ManyToOne
    private PlSpecials specId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PlCources id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVacantCount() {
        return this.vacantCount;
    }

    public PlCources vacantCount(Integer vacantCount) {
        this.setVacantCount(vacantCount);
        return this;
    }

    public void setVacantCount(Integer vacantCount) {
        this.vacantCount = vacantCount;
    }

    public String getNameUz() {
        return this.nameUz;
    }

    public PlCources nameUz(String nameUz) {
        this.setNameUz(nameUz);
        return this;
    }

    public void setNameUz(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getDescriptionUz() {
        return this.descriptionUz;
    }

    public PlCources descriptionUz(String descriptionUz) {
        this.setDescriptionUz(descriptionUz);
        return this;
    }

    public void setDescriptionUz(String descriptionUz) {
        this.descriptionUz = descriptionUz;
    }

    public String getNameRu() {
        return this.nameRu;
    }

    public PlCources nameRu(String nameRu) {
        this.setNameRu(nameRu);
        return this;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getDescriptionRu() {
        return this.descriptionRu;
    }

    public PlCources descriptionRu(String descriptionRu) {
        this.setDescriptionRu(descriptionRu);
        return this;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
    }

    public LocalDate getStartDate() {
        return this.startDate;
    }

    public PlCources startDate(LocalDate startDate) {
        this.setStartDate(startDate);
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return this.endDate;
    }

    public PlCources endDate(LocalDate endDate) {
        this.setEndDate(endDate);
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public PlCources isActive(Integer isActive) {
        this.setIsActive(isActive);
        return this;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Set<PlCourceServices> getCourceIds() {
        return this.courceIds;
    }

    public void setCourceIds(Set<PlCourceServices> plCourceServices) {
        if (this.courceIds != null) {
            this.courceIds.forEach(i -> i.setPlCources(null));
        }
        if (plCourceServices != null) {
            plCourceServices.forEach(i -> i.setPlCources(this));
        }
        this.courceIds = plCourceServices;
    }

    public PlCources courceIds(Set<PlCourceServices> plCourceServices) {
        this.setCourceIds(plCourceServices);
        return this;
    }

    public PlCources addCourceId(PlCourceServices plCourceServices) {
        this.courceIds.add(plCourceServices);
        plCourceServices.setPlCources(this);
        return this;
    }

    public PlCources removeCourceId(PlCourceServices plCourceServices) {
        this.courceIds.remove(plCourceServices);
        plCourceServices.setPlCources(null);
        return this;
    }

    public Set<PlRegion> getRegionIds() {
        return this.regionIds;
    }

    public void setRegionIds(Set<PlRegion> plRegions) {
        if (this.regionIds != null) {
            this.regionIds.forEach(i -> i.setPlCources(null));
        }
        if (plRegions != null) {
            plRegions.forEach(i -> i.setPlCources(this));
        }
        this.regionIds = plRegions;
    }

    public PlCources regionIds(Set<PlRegion> plRegions) {
        this.setRegionIds(plRegions);
        return this;
    }

    public PlCources addRegionId(PlRegion plRegion) {
        this.regionIds.add(plRegion);
        plRegion.setPlCources(this);
        return this;
    }

    public PlCources removeRegionId(PlRegion plRegion) {
        this.regionIds.remove(plRegion);
        plRegion.setPlCources(null);
        return this;
    }

    public Set<PlDistrict> getDistrictIds() {
        return this.districtIds;
    }

    public void setDistrictIds(Set<PlDistrict> plDistricts) {
        if (this.districtIds != null) {
            this.districtIds.forEach(i -> i.setPlCources(null));
        }
        if (plDistricts != null) {
            plDistricts.forEach(i -> i.setPlCources(this));
        }
        this.districtIds = plDistricts;
    }

    public PlCources districtIds(Set<PlDistrict> plDistricts) {
        this.setDistrictIds(plDistricts);
        return this;
    }

    public PlCources addDistrictId(PlDistrict plDistrict) {
        this.districtIds.add(plDistrict);
        plDistrict.setPlCources(this);
        return this;
    }

    public PlCources removeDistrictId(PlDistrict plDistrict) {
        this.districtIds.remove(plDistrict);
        plDistrict.setPlCources(null);
        return this;
    }

    public PlCompany getCompanyId() {
        return this.companyId;
    }

    public void setCompanyId(PlCompany plCompany) {
        this.companyId = plCompany;
    }

    public PlCources companyId(PlCompany plCompany) {
        this.setCompanyId(plCompany);
        return this;
    }

    public PlSpecials getSpecId() {
        return this.specId;
    }

    public void setSpecId(PlSpecials plSpecials) {
        this.specId = plSpecials;
    }

    public PlCources specId(PlSpecials plSpecials) {
        this.setSpecId(plSpecials);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlCources)) {
            return false;
        }
        return id != null && id.equals(((PlCources) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PlCources{" +
            "id=" + getId() +
            ", vacantCount=" + getVacantCount() +
            ", nameUz='" + getNameUz() + "'" +
            ", descriptionUz='" + getDescriptionUz() + "'" +
            ", nameRu='" + getNameRu() + "'" +
            ", descriptionRu='" + getDescriptionRu() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", isActive=" + getIsActive() +
            "}";
    }
}
