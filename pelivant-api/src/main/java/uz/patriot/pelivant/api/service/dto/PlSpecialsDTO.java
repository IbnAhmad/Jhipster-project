package uz.patriot.pelivant.api.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link uz.patriot.pelivant.api.domain.PlSpecials} entity.
 */
public class PlSpecialsDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 200)
    private String nameUz;

    @NotNull
    @Size(max = 2000)
    private String descriptionUz;

    @NotNull
    @Size(max = 200)
    private String nameRu;

    @NotNull
    @Size(max = 2000)
    private String descriptionRu;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameUz() {
        return nameUz;
    }

    public void setNameUz(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getDescriptionUz() {
        return descriptionUz;
    }

    public void setDescriptionUz(String descriptionUz) {
        this.descriptionUz = descriptionUz;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getDescriptionRu() {
        return descriptionRu;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlSpecialsDTO)) {
            return false;
        }

        PlSpecialsDTO plSpecialsDTO = (PlSpecialsDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, plSpecialsDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PlSpecialsDTO{" +
            "id=" + getId() +
            ", nameUz='" + getNameUz() + "'" +
            ", descriptionUz='" + getDescriptionUz() + "'" +
            ", nameRu='" + getNameRu() + "'" +
            ", descriptionRu='" + getDescriptionRu() + "'" +
            "}";
    }
}
