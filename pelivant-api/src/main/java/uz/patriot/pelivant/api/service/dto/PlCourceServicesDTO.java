package uz.patriot.pelivant.api.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link uz.patriot.pelivant.api.domain.PlCourceServices} entity.
 */
public class PlCourceServicesDTO implements Serializable {

    private Long id;

    @NotNull
    private String mxikCode;

    @NotNull
    @Size(max = 200)
    private String name;

    @NotNull
    private Integer isActive;

    @NotNull
    private LocalDate startDate;

    @NotNull
    private LocalDate endDate;

    @NotNull
    private Double cost;

    private PlCourcesDTO plCources;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMxikCode() {
        return mxikCode;
    }

    public void setMxikCode(String mxikCode) {
        this.mxikCode = mxikCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public PlCourcesDTO getPlCources() {
        return plCources;
    }

    public void setPlCources(PlCourcesDTO plCources) {
        this.plCources = plCources;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlCourceServicesDTO)) {
            return false;
        }

        PlCourceServicesDTO plCourceServicesDTO = (PlCourceServicesDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, plCourceServicesDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PlCourceServicesDTO{" +
            "id=" + getId() +
            ", mxikCode='" + getMxikCode() + "'" +
            ", name='" + getName() + "'" +
            ", isActive=" + getIsActive() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", cost=" + getCost() +
            ", plCources=" + getPlCources() +
            "}";
    }
}
