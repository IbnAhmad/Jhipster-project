package uz.patriot.pelivant.api.service.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import uz.patriot.pelivant.api.domain.PlSpecials;
import uz.patriot.pelivant.api.service.dto.PlSpecialsDTO;

/**
 * Mapper for the entity {@link PlSpecials} and its DTO {@link PlSpecialsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PlSpecialsMapper extends EntityMapper<PlSpecialsDTO, PlSpecials> {
    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PlSpecialsDTO toDtoId(PlSpecials plSpecials);
}
