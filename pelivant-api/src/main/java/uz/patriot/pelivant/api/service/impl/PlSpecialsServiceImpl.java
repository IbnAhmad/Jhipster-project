package uz.patriot.pelivant.api.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.patriot.pelivant.api.domain.PlSpecials;
import uz.patriot.pelivant.api.repository.PlSpecialsRepository;
import uz.patriot.pelivant.api.service.PlSpecialsService;
import uz.patriot.pelivant.api.service.dto.PlSpecialsDTO;
import uz.patriot.pelivant.api.service.mapper.PlSpecialsMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PlSpecials}.
 */
@Service
@Transactional
public class PlSpecialsServiceImpl implements PlSpecialsService {

    private final Logger log = LoggerFactory.getLogger(PlSpecialsServiceImpl.class);

    private final PlSpecialsRepository plSpecialsRepository;

    private final PlSpecialsMapper plSpecialsMapper;

    public PlSpecialsServiceImpl(PlSpecialsRepository plSpecialsRepository, PlSpecialsMapper plSpecialsMapper) {
        this.plSpecialsRepository = plSpecialsRepository;
        this.plSpecialsMapper = plSpecialsMapper;
    }

    @Override
    public PlSpecialsDTO save(PlSpecialsDTO plSpecialsDTO) {
        log.debug("Request to save PlSpecials : {}", plSpecialsDTO);
        PlSpecials plSpecials = plSpecialsMapper.toEntity(plSpecialsDTO);
        plSpecials = plSpecialsRepository.save(plSpecials);
        return plSpecialsMapper.toDto(plSpecials);
    }

    @Override
    public Optional<PlSpecialsDTO> partialUpdate(PlSpecialsDTO plSpecialsDTO) {
        log.debug("Request to partially update PlSpecials : {}", plSpecialsDTO);

        return plSpecialsRepository
            .findById(plSpecialsDTO.getId())
            .map(existingPlSpecials -> {
                plSpecialsMapper.partialUpdate(existingPlSpecials, plSpecialsDTO);

                return existingPlSpecials;
            })
            .map(plSpecialsRepository::save)
            .map(plSpecialsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PlSpecialsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PlSpecials");
        return plSpecialsRepository.findAll(pageable).map(plSpecialsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PlSpecialsDTO> findOne(Long id) {
        log.debug("Request to get PlSpecials : {}", id);
        return plSpecialsRepository.findById(id).map(plSpecialsMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlSpecials : {}", id);
        plSpecialsRepository.deleteById(id);
    }
}
