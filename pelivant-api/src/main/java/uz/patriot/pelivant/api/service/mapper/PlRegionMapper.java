package uz.patriot.pelivant.api.service.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import uz.patriot.pelivant.api.domain.PlRegion;
import uz.patriot.pelivant.api.service.dto.PlRegionDTO;

/**
 * Mapper for the entity {@link PlRegion} and its DTO {@link PlRegionDTO}.
 */
@Mapper(componentModel = "spring", uses = { PlCourcesMapper.class })
public interface PlRegionMapper extends EntityMapper<PlRegionDTO, PlRegion> {
    @Mapping(target = "plCources", source = "plCources", qualifiedByName = "id")
    PlRegionDTO toDto(PlRegion s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PlRegionDTO toDtoId(PlRegion plRegion);
}
