package uz.patriot.pelivant.api.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import uz.patriot.pelivant.api.domain.PlCourceServices;
import uz.patriot.pelivant.api.service.dto.PlCourceServicesDTO;

/**
 * Mapper for the entity {@link PlCourceServices} and its DTO {@link PlCourceServicesDTO}.
 */
@Mapper(componentModel = "spring", uses = { PlCourcesMapper.class })
public interface PlCourceServicesMapper extends EntityMapper<PlCourceServicesDTO, PlCourceServices> {
    @Mapping(target = "plCources", source = "plCources", qualifiedByName = "id")
    PlCourceServicesDTO toDto(PlCourceServices s);
}
