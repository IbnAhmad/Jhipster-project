package uz.patriot.pelivant.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.patriot.pelivant.api.domain.PlDistrict;

/**
 * Spring Data SQL repository for the PlDistrict entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlDistrictRepository extends JpaRepository<PlDistrict, Long> {}
