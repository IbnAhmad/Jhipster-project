package uz.patriot.pelivant.api.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link uz.patriot.pelivant.api.domain.PlCompany} entity.
 */
public class PlCompanyDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 200)
    private String name;

    @NotNull
    @Size(max = 2000)
    private String descriptionUz;

    @NotNull
    @Size(max = 2000)
    private String descriptionRu;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptionUz() {
        return descriptionUz;
    }

    public void setDescriptionUz(String descriptionUz) {
        this.descriptionUz = descriptionUz;
    }

    public String getDescriptionRu() {
        return descriptionRu;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlCompanyDTO)) {
            return false;
        }

        PlCompanyDTO plCompanyDTO = (PlCompanyDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, plCompanyDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PlCompanyDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", descriptionUz='" + getDescriptionUz() + "'" +
            ", descriptionRu='" + getDescriptionRu() + "'" +
            "}";
    }
}
