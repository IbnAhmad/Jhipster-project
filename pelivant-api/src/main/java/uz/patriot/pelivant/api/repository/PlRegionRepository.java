package uz.patriot.pelivant.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.patriot.pelivant.api.domain.PlRegion;

/**
 * Spring Data SQL repository for the PlRegion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlRegionRepository extends JpaRepository<PlRegion, Long> {}
