package uz.patriot.pelivant.api.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * A PlCompany.
 */
@Entity
@Table(name = "pl_company")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PlCompany implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(max = 200)
    @Column(name = "name", length = 200, nullable = false)
    private String name;

    @NotNull
    @Size(max = 2000)
    @Column(name = "description_uz", length = 2000, nullable = false)
    private String descriptionUz;

    @NotNull
    @Size(max = 2000)
    @Column(name = "description_ru", length = 2000, nullable = false)
    private String descriptionRu;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PlCompany id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public PlCompany name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptionUz() {
        return this.descriptionUz;
    }

    public PlCompany descriptionUz(String descriptionUz) {
        this.setDescriptionUz(descriptionUz);
        return this;
    }

    public void setDescriptionUz(String descriptionUz) {
        this.descriptionUz = descriptionUz;
    }

    public String getDescriptionRu() {
        return this.descriptionRu;
    }

    public PlCompany descriptionRu(String descriptionRu) {
        this.setDescriptionRu(descriptionRu);
        return this;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlCompany)) {
            return false;
        }
        return id != null && id.equals(((PlCompany) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PlCompany{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", descriptionUz='" + getDescriptionUz() + "'" +
            ", descriptionRu='" + getDescriptionRu() + "'" +
            "}";
    }
}
