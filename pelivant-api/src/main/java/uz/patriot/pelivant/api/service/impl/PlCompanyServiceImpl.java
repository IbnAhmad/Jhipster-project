package uz.patriot.pelivant.api.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.patriot.pelivant.api.domain.PlCompany;
import uz.patriot.pelivant.api.repository.PlCompanyRepository;
import uz.patriot.pelivant.api.service.PlCompanyService;
import uz.patriot.pelivant.api.service.dto.PlCompanyDTO;
import uz.patriot.pelivant.api.service.mapper.PlCompanyMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PlCompany}.
 */
@Service
@Transactional
public class PlCompanyServiceImpl implements PlCompanyService {

    private final Logger log = LoggerFactory.getLogger(PlCompanyServiceImpl.class);

    private final PlCompanyRepository plCompanyRepository;

    private final PlCompanyMapper plCompanyMapper;

    public PlCompanyServiceImpl(PlCompanyRepository plCompanyRepository, PlCompanyMapper plCompanyMapper) {
        this.plCompanyRepository = plCompanyRepository;
        this.plCompanyMapper = plCompanyMapper;
    }

    @Override
    public PlCompanyDTO save(PlCompanyDTO plCompanyDTO) {
        log.debug("Request to save PlCompany : {}", plCompanyDTO);
        PlCompany plCompany = plCompanyMapper.toEntity(plCompanyDTO);
        plCompany = plCompanyRepository.save(plCompany);
        return plCompanyMapper.toDto(plCompany);
    }

    @Override
    public Optional<PlCompanyDTO> partialUpdate(PlCompanyDTO plCompanyDTO) {
        log.debug("Request to partially update PlCompany : {}", plCompanyDTO);

        return plCompanyRepository
            .findById(plCompanyDTO.getId())
            .map(existingPlCompany -> {
                plCompanyMapper.partialUpdate(existingPlCompany, plCompanyDTO);

                return existingPlCompany;
            })
            .map(plCompanyRepository::save)
            .map(plCompanyMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PlCompanyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PlCompanies");
        return plCompanyRepository.findAll(pageable).map(plCompanyMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PlCompanyDTO> findOne(Long id) {
        log.debug("Request to get PlCompany : {}", id);
        return plCompanyRepository.findById(id).map(plCompanyMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlCompany : {}", id);
        plCompanyRepository.deleteById(id);
    }
}
