package uz.patriot.pelivant.api.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import uz.patriot.pelivant.api.service.dto.PlSpecialsDTO;

import java.util.Optional;

/**
 * Service Interface for managing {@link uz.patriot.pelivant.api.domain.PlSpecials}.
 */
public interface PlSpecialsService {
    /**
     * Save a plSpecials.
     *
     * @param plSpecialsDTO the entity to save.
     * @return the persisted entity.
     */
    PlSpecialsDTO save(PlSpecialsDTO plSpecialsDTO);

    /**
     * Partially updates a plSpecials.
     *
     * @param plSpecialsDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PlSpecialsDTO> partialUpdate(PlSpecialsDTO plSpecialsDTO);

    /**
     * Get all the plSpecials.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PlSpecialsDTO> findAll(Pageable pageable);

    /**
     * Get the "id" plSpecials.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PlSpecialsDTO> findOne(Long id);

    /**
     * Delete the "id" plSpecials.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
