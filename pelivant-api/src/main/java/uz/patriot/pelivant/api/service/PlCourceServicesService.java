package uz.patriot.pelivant.api.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import uz.patriot.pelivant.api.service.dto.PlCourceServicesDTO;

import java.util.Optional;

/**
 * Service Interface for managing {@link uz.patriot.pelivant.api.domain.PlCourceServices}.
 */
public interface PlCourceServicesService {
    /**
     * Save a plCourceServices.
     *
     * @param plCourceServicesDTO the entity to save.
     * @return the persisted entity.
     */
    PlCourceServicesDTO save(PlCourceServicesDTO plCourceServicesDTO);

    /**
     * Partially updates a plCourceServices.
     *
     * @param plCourceServicesDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PlCourceServicesDTO> partialUpdate(PlCourceServicesDTO plCourceServicesDTO);

    /**
     * Get all the plCourceServices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PlCourceServicesDTO> findAll(Pageable pageable);

    /**
     * Get the "id" plCourceServices.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PlCourceServicesDTO> findOne(Long id);

    /**
     * Delete the "id" plCourceServices.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
