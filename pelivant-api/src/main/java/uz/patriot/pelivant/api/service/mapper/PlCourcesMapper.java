package uz.patriot.pelivant.api.service.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import uz.patriot.pelivant.api.domain.PlCources;
import uz.patriot.pelivant.api.service.dto.PlCourcesDTO;

/**
 * Mapper for the entity {@link PlCources} and its DTO {@link PlCourcesDTO}.
 */
@Mapper(componentModel = "spring", uses = { PlCompanyMapper.class, PlSpecialsMapper.class })
public interface PlCourcesMapper extends EntityMapper<PlCourcesDTO, PlCources> {
    @Mapping(target = "companyId", source = "companyId", qualifiedByName = "id")
    @Mapping(target = "specId", source = "specId", qualifiedByName = "id")
    PlCourcesDTO toDto(PlCources s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PlCourcesDTO toDtoId(PlCources plCources);
}
