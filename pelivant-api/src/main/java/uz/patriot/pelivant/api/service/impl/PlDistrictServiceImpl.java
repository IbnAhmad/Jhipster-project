package uz.patriot.pelivant.api.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.patriot.pelivant.api.domain.PlDistrict;
import uz.patriot.pelivant.api.repository.PlDistrictRepository;
import uz.patriot.pelivant.api.service.PlDistrictService;
import uz.patriot.pelivant.api.service.dto.PlDistrictDTO;
import uz.patriot.pelivant.api.service.mapper.PlDistrictMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PlDistrict}.
 */
@Service
@Transactional
public class PlDistrictServiceImpl implements PlDistrictService {

    private final Logger log = LoggerFactory.getLogger(PlDistrictServiceImpl.class);

    private final PlDistrictRepository plDistrictRepository;

    private final PlDistrictMapper plDistrictMapper;

    public PlDistrictServiceImpl(PlDistrictRepository plDistrictRepository, PlDistrictMapper plDistrictMapper) {
        this.plDistrictRepository = plDistrictRepository;
        this.plDistrictMapper = plDistrictMapper;
    }

    @Override
    public PlDistrictDTO save(PlDistrictDTO plDistrictDTO) {
        log.debug("Request to save PlDistrict : {}", plDistrictDTO);
        PlDistrict plDistrict = plDistrictMapper.toEntity(plDistrictDTO);
        plDistrict = plDistrictRepository.save(plDistrict);
        return plDistrictMapper.toDto(plDistrict);
    }

    @Override
    public Optional<PlDistrictDTO> partialUpdate(PlDistrictDTO plDistrictDTO) {
        log.debug("Request to partially update PlDistrict : {}", plDistrictDTO);

        return plDistrictRepository
            .findById(plDistrictDTO.getId())
            .map(existingPlDistrict -> {
                plDistrictMapper.partialUpdate(existingPlDistrict, plDistrictDTO);

                return existingPlDistrict;
            })
            .map(plDistrictRepository::save)
            .map(plDistrictMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PlDistrictDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PlDistricts");
        return plDistrictRepository.findAll(pageable).map(plDistrictMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PlDistrictDTO> findOne(Long id) {
        log.debug("Request to get PlDistrict : {}", id);
        return plDistrictRepository.findById(id).map(plDistrictMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlDistrict : {}", id);
        plDistrictRepository.deleteById(id);
    }
}
