package uz.patriot.pelivant.api.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.patriot.pelivant.api.domain.PlCources;
import uz.patriot.pelivant.api.repository.PlCourcesRepository;
import uz.patriot.pelivant.api.service.PlCourcesService;
import uz.patriot.pelivant.api.service.dto.PlCourcesDTO;
import uz.patriot.pelivant.api.service.mapper.PlCourcesMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PlCources}.
 */
@Service
@Transactional
public class PlCourcesServiceImpl implements PlCourcesService {

    private final Logger log = LoggerFactory.getLogger(PlCourcesServiceImpl.class);

    private final PlCourcesRepository plCourcesRepository;

    private final PlCourcesMapper plCourcesMapper;

    public PlCourcesServiceImpl(PlCourcesRepository plCourcesRepository, PlCourcesMapper plCourcesMapper) {
        this.plCourcesRepository = plCourcesRepository;
        this.plCourcesMapper = plCourcesMapper;
    }

    @Override
    public PlCourcesDTO save(PlCourcesDTO plCourcesDTO) {
        log.debug("Request to save PlCources : {}", plCourcesDTO);
        PlCources plCources = plCourcesMapper.toEntity(plCourcesDTO);
        plCources = plCourcesRepository.save(plCources);
        return plCourcesMapper.toDto(plCources);
    }

    @Override
    public Optional<PlCourcesDTO> partialUpdate(PlCourcesDTO plCourcesDTO) {
        log.debug("Request to partially update PlCources : {}", plCourcesDTO);

        return plCourcesRepository
            .findById(plCourcesDTO.getId())
            .map(existingPlCources -> {
                plCourcesMapper.partialUpdate(existingPlCources, plCourcesDTO);

                return existingPlCources;
            })
            .map(plCourcesRepository::save)
            .map(plCourcesMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PlCourcesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PlCources");
        return plCourcesRepository.findAll(pageable).map(plCourcesMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PlCourcesDTO> findOne(Long id) {
        log.debug("Request to get PlCources : {}", id);
        return plCourcesRepository.findById(id).map(plCourcesMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlCources : {}", id);
        plCourcesRepository.deleteById(id);
    }
}
