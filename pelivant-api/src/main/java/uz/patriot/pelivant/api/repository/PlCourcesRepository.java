package uz.patriot.pelivant.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.patriot.pelivant.api.domain.PlCources;

/**
 * Spring Data SQL repository for the PlCources entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlCourcesRepository extends JpaRepository<PlCources, Long> {}
