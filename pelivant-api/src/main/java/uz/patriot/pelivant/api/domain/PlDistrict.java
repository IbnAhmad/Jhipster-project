package uz.patriot.pelivant.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * A PlDistrict.
 */
@Entity
@Table(name = "pl_district")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PlDistrict implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(max = 200)
    @Column(name = "name_uz", length = 200, nullable = false)
    private String nameUz;

    @NotNull
    @Size(max = 2000)
    @Column(name = "description_uz", length = 2000, nullable = false)
    private String descriptionUz;

    @NotNull
    @Size(max = 200)
    @Column(name = "name_ru", length = 200, nullable = false)
    private String nameRu;

    @NotNull
    @Size(max = 2000)
    @Column(name = "description_ru", length = 2000, nullable = false)
    private String descriptionRu;

    @ManyToOne
    @JsonIgnoreProperties(value = { "regionIds", "plCources" }, allowSetters = true)
    private PlRegion plRegion;

    @ManyToOne
    @JsonIgnoreProperties(value = { "courceIds", "regionIds", "districtIds", "companyId", "specId" }, allowSetters = true)
    private PlCources plCources;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PlDistrict id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameUz() {
        return this.nameUz;
    }

    public PlDistrict nameUz(String nameUz) {
        this.setNameUz(nameUz);
        return this;
    }

    public void setNameUz(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getDescriptionUz() {
        return this.descriptionUz;
    }

    public PlDistrict descriptionUz(String descriptionUz) {
        this.setDescriptionUz(descriptionUz);
        return this;
    }

    public void setDescriptionUz(String descriptionUz) {
        this.descriptionUz = descriptionUz;
    }

    public String getNameRu() {
        return this.nameRu;
    }

    public PlDistrict nameRu(String nameRu) {
        this.setNameRu(nameRu);
        return this;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getDescriptionRu() {
        return this.descriptionRu;
    }

    public PlDistrict descriptionRu(String descriptionRu) {
        this.setDescriptionRu(descriptionRu);
        return this;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
    }

    public PlRegion getPlRegion() {
        return this.plRegion;
    }

    public void setPlRegion(PlRegion plRegion) {
        this.plRegion = plRegion;
    }

    public PlDistrict plRegion(PlRegion plRegion) {
        this.setPlRegion(plRegion);
        return this;
    }

    public PlCources getPlCources() {
        return this.plCources;
    }

    public void setPlCources(PlCources plCources) {
        this.plCources = plCources;
    }

    public PlDistrict plCources(PlCources plCources) {
        this.setPlCources(plCources);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlDistrict)) {
            return false;
        }
        return id != null && id.equals(((PlDistrict) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PlDistrict{" +
            "id=" + getId() +
            ", nameUz='" + getNameUz() + "'" +
            ", descriptionUz='" + getDescriptionUz() + "'" +
            ", nameRu='" + getNameRu() + "'" +
            ", descriptionRu='" + getDescriptionRu() + "'" +
            "}";
    }
}
