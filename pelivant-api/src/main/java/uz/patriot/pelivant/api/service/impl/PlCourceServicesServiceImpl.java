package uz.patriot.pelivant.api.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.patriot.pelivant.api.domain.PlCourceServices;
import uz.patriot.pelivant.api.repository.PlCourceServicesRepository;
import uz.patriot.pelivant.api.service.PlCourceServicesService;
import uz.patriot.pelivant.api.service.dto.PlCourceServicesDTO;
import uz.patriot.pelivant.api.service.mapper.PlCourceServicesMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PlCourceServices}.
 */
@Service
@Transactional
public class PlCourceServicesServiceImpl implements PlCourceServicesService {

    private final Logger log = LoggerFactory.getLogger(PlCourceServicesServiceImpl.class);

    private final PlCourceServicesRepository plCourceServicesRepository;

    private final PlCourceServicesMapper plCourceServicesMapper;

    public PlCourceServicesServiceImpl(
        PlCourceServicesRepository plCourceServicesRepository,
        PlCourceServicesMapper plCourceServicesMapper
    ) {
        this.plCourceServicesRepository = plCourceServicesRepository;
        this.plCourceServicesMapper = plCourceServicesMapper;
    }

    @Override
    public PlCourceServicesDTO save(PlCourceServicesDTO plCourceServicesDTO) {
        log.debug("Request to save PlCourceServices : {}", plCourceServicesDTO);
        PlCourceServices plCourceServices = plCourceServicesMapper.toEntity(plCourceServicesDTO);
        plCourceServices = plCourceServicesRepository.save(plCourceServices);
        return plCourceServicesMapper.toDto(plCourceServices);
    }

    @Override
    public Optional<PlCourceServicesDTO> partialUpdate(PlCourceServicesDTO plCourceServicesDTO) {
        log.debug("Request to partially update PlCourceServices : {}", plCourceServicesDTO);

        return plCourceServicesRepository
            .findById(plCourceServicesDTO.getId())
            .map(existingPlCourceServices -> {
                plCourceServicesMapper.partialUpdate(existingPlCourceServices, plCourceServicesDTO);

                return existingPlCourceServices;
            })
            .map(plCourceServicesRepository::save)
            .map(plCourceServicesMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PlCourceServicesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PlCourceServices");
        return plCourceServicesRepository.findAll(pageable).map(plCourceServicesMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PlCourceServicesDTO> findOne(Long id) {
        log.debug("Request to get PlCourceServices : {}", id);
        return plCourceServicesRepository.findById(id).map(plCourceServicesMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlCourceServices : {}", id);
        plCourceServicesRepository.deleteById(id);
    }
}
