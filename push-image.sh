#!/bin/bash

PROJECT=`echo ${3//./} | tr '[:upper:]' '[:lower:]'`
IMAGE_NAME=`echo $1/${PROJECT}:$2`

docker build --build-arg PROJECT_NAME=$PROJECT -f $PROJECT/Dockerfile -t $PROJECT .
docker tag $PROJECT $IMAGE_NAME
docker push $IMAGE_NAME
