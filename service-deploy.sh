#!/bin/bash

PROJECT=`echo ${6//./} | tr '[:upper:]' '[:lower:]'`

FOLDER=`echo audit_${PROJECT}`
IMAGENAME=`echo $3/${PROJECT}:$4`

mkdir -p ~/.ssh
chmod 700 ~/.ssh
eval $(ssh-agent -s)
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
ssh-add <(echo "$2")

ssh -oStrictHostKeyChecking=no $7@$1 mkdir $FOLDER -p
scp -oStrictHostKeyChecking=no $6/docker-compose.$5.yml $7@$1:$FOLDER/
#ssh -oStrictHostKeyChecking=no $7@$1 env IMAGE=$IMAGENAME docker-compose -f $FOLDER/docker-compose.$5.yml up -d --force-recreate
ssh -oStrictHostKeyChecking=no $7@$1 env IMAGE=$IMAGENAME docker stack deploy -c $FOLDER/docker-compose.$5.yml $8
